import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { MediaObserver } from '@angular/flex-layout';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { Product } from '../../shared/services';

@Component({
  selector: 'nga-product-grid',
  styleUrls: [ './product-grid.component.scss' ],
  templateUrl: './product-grid.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ProductGridComponent {
  @Input() products: Product[];
  readonly columns$: Observable<number>;
  readonly breakpoints = new Map([
    [ 'xs', 1 ],
    [ 'sm', 2 ],
    [ 'md', 3 ],
    [ 'lg', 4 ],
    [ 'xl', 5 ],
  ]);

  constructor(private media: MediaObserver) {
    // If the initial screen size is xs ObservableMedia doesn't emit an event
    // and grid-list rendering fails.
    // slight change to use MediaObserver observable
    // indicates the current width of the user's device viewport/media query size (mqAlias)
    this.columns$ = this.media.media$.pipe(map(mc => this.breakpoints.get(mc.mqAlias)));
  }
}
